﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Worker.Models;

namespace Worker
{
    public partial class Service1 : ServiceBase
    {
        Thread th;
        bool isRunning = false;
        //private static AwsConfig _awsConfig;
        private static AmazonSQSClient _client;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            th = new Thread(ListenQueue);
            th.Start();
            isRunning = true;
        }

        private void ListenQueue()
        {
            while (isRunning)
            {
                var receivedMessage = Receive();
                if (receivedMessage != null)
                {
                    try
                    {
                        DeleteMessageFromQueue(receivedMessage.ReceiptHandle);

                        if ("sms".Equals(receivedMessage.Body.deliveryMethod))
                        {
                            SendSms(receivedMessage);
                        }
                        else if ("email".Equals(receivedMessage.Body.deliveryMethod))
                        {
                            SendEmail(receivedMessage);
                        }
                        else
                        {
                            throw new Exception(string.Format("Recipe Handler {0} Undefined delivery method {1}.", receivedMessage.ReceiptHandle, receivedMessage.Body.deliveryMethod));
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        //write exception to log and send message to debug queue
                        SendMessageToDebug(receivedMessage);
                    }
                }

                Thread.Sleep(1000);
                
            }
        }

        protected override void OnStop()
        {
            isRunning = false;
            th = null;
        }

        private static ReceivedMessage Receive()
        {
            /*
            _awsConfig = AwsConfig.Read();
            _client = _awsConfig.CreateSQSClient();
            */
            _client = new AmazonSQSClient();

            var request = new ReceiveMessageRequest
            {
                QueueUrl = ConfigurationManager.AppSettings["QueueUrl"],
                MaxNumberOfMessages = 1
            };
            var response = _client.ReceiveMessage(request);

            if (response.Messages.Count == 0) return null;
            var message = response.Messages[0];
            var json = message.Body;
            return new ReceivedMessage
            {
                Body = JsonConvert.DeserializeObject<NotificationModel>(json),
                ReceiptHandle = message.ReceiptHandle
            };
        }
        private static void DeleteMessageFromQueue(string receiptHandle)
        {
            var request = new DeleteMessageRequest(
                queueUrl: ConfigurationManager.AppSettings["QueueUrl"],
                receiptHandle: receiptHandle);
            var response = _client.DeleteMessage(request);
        }

        private static void SendMessageToDebug (ReceivedMessage receivedMessage)
        {
            var request = new SendMessageRequest(
                queueUrl: ConfigurationManager.AppSettings["DebugQueueUrl"],
                messageBody: JsonConvert.SerializeObject(receivedMessage.Body));

            var response = _client.SendMessage(request);
        }

        private static void SendSms(ReceivedMessage received)
        {
            string twilioSid = ConfigurationManager.AppSettings["TwilioSid"];
            string twilioToken = ConfigurationManager.AppSettings["TwilioToken"];
            string twilioCallerNum = ConfigurationManager.AppSettings["TwilioCallerNum"];
            try
            {
                TwilioClient.Init(twilioSid, twilioToken);

                var message = MessageResource.Create(
                    to: new PhoneNumber(received.Body.recipients.to),
                    from: new PhoneNumber(twilioCallerNum),
                    body: received.Body.message.body);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void SendEmail(ReceivedMessage received)
        {
            var FROM = received.Body.recipients.from;
            var TO = received.Body.recipients.to; 

            var SUBJECT = received.Body.message.subject;
            var BODY = received.Body.message.body;
            
            var SMTP_USERNAME = ConfigurationManager.AppSettings["SmtpUserName"]; 
            var SMTP_PASSWORD = ConfigurationManager.AppSettings["SmtpPassword"];
            
            var HOST = ConfigurationManager.AppSettings["SmtpHost"];

           int PORT = Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]);

            // Create an SMTP client with the specified host name and port.
            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                client.EnableSsl = true;
                try
                {
                    client.Send(FROM, TO, SUBJECT, BODY);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

        }
    }
}
