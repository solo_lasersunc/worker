﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Worker.Models
{
    public class ReceivedMessage
    {
        public NotificationModel Body { get; set; }
        public string ReceiptHandle { get; set; }
    }
}
