﻿
namespace Worker.Models
{
    public class Message
    {
        public string body { get; set; }
        public string subject { get; set; }
    }
}