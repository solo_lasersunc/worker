﻿using Amazon.SQS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Worker
{
    public class AwsConfig
    {
        private AwsConfig()
        {

        }

        public string AccessKeyId { get; set; }
        public string SecretKey { get; set; }
        public string QueueUrl { get; set; }
        public string DebugQueueUrl { get; set; }

        public static AwsConfig Read()
        {
            var config = new AwsConfig();
            config.AccessKeyId = ConfigurationManager.AppSettings["AccessKeyId"];
            config.SecretKey = ConfigurationManager.AppSettings["SecretKey"];
            config.QueueUrl = ConfigurationManager.AppSettings["QueueUrl"];
            config.DebugQueueUrl = ConfigurationManager.AppSettings["DebugQueueUrl"];
            return config;
            //return JsonConvert.DeserializeObject<AwsConfig>(json);
            
        }

        public AmazonSQSClient CreateSQSClient()
        {
            return new AmazonSQSClient(
                awsAccessKeyId: AccessKeyId,
                awsSecretAccessKey: SecretKey,
                region: Amazon.RegionEndpoint.APSoutheast1);
        }
    }
}
