﻿
namespace Worker.Models
{
    public class NotificationModel
    {
        public Recipient recipients { get; set; }
        public Message message { get; set; }
        public string deliveryMethod { get; set; }
    }
}