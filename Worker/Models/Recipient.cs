﻿
namespace Worker.Models
{
    public class Recipient
    {
        public string to { get; set; }
        public string from { get; set; }
    }
}